-- Create the database if it doesnt exist already
IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'PostgradDb')
BEGIN
  CREATE DATABASE PostgradDb;
END;

USE PostgradDb;
GO
-- Next is tables. When creating the tables, we need to be careful about FKs.
-- If we need to reference another table, it needs to exist first.
-- E.g. Student needs Professor Id, so Professor needs to exist first.
-- The order of creation will be: Professor, Student, Project, Subject, StudentSubject

DROP TABLE StudentSubject, Subject, Project, Student, Professor; -- First drop the tables if they exist.

CREATE TABLE Professor (
    Id int IDENTITY(1,1) PRIMARY KEY,
    [Name] varchar(50) NOT NULL,
    Field varchar(50)
);

CREATE TABLE Student (
    Id int IDENTITY(1,1) PRIMARY KEY,
    [Name] varchar(50) NOT NULL,
    SupervisorId int NOT NULL REFERENCES Professor(Id)
);

CREATE TABLE Project (
    Id int IDENTITY(1,1) PRIMARY KEY,
    Title varchar(100) NOT NULL,
	StudentId int NOT NULL REFERENCES Student(Id)
);

CREATE TABLE Subject (
    Id int IDENTITY(1,1) PRIMARY KEY,
    SubCode varchar(10) NOT NULL,
    SubTitle varchar(100) NOT NULL,
    LecturerId int NOT NULL REFERENCES Professor(Id)
);

CREATE TABLE StudentSubject (
    StudId int REFERENCES Student(Id),
    SubId int REFERENCES Subject(Id),
    PRIMARY KEY (StudId, SubId)
);

-- Now we can add some data

-- Professors
INSERT INTO Professor ([Name], Field) VALUES
('William Fences', 'Monopoly'), -- 1
('Arvid Str�m', 'High voltage electricity'), -- 2
('Anne Hansen', 'Soldering while skydiving'), -- 3
('Liv Larsen', 'Anti aging'); -- 4
-- Students
INSERT INTO Student ([Name], SupervisorId) VALUES 
('Ola Nordmann', 1), -- 1
('Emma Hansen', 1), -- 2
('Olivia Nordmann', 2), -- 3
('Lucas Olsen', 2), -- 4
('Askel Nilsen', 3), -- 5
('Frida Kristiansen', 3), -- 6
('Ingrid Johansen', 3), -- 7
('Jakob Andersen', 3); -- 8

-- Projects
INSERT INTO Project (Title, StudentId) VALUES 
('Automating your life in 3 easy steps', 1),
('How to build a technology empire', 2),
('Powering the new age', 3),
('Predicting the weather on Mars', 5),
('Classifying Deep Learning classifiers', 7),
('Can we make safe bets? An analysis of card counting', 8);

-- Subjects
INSERT INTO Subject (SubCode, SubTitle, LecturerId) VALUES
('ONT4001', 'Advanced Programming', 1), -- 1
('ELEC1001', 'Electroboom', 2), -- 2 
('SKY2003', 'Tig Welding for astronauts', 3), -- 3
('AGED4320', 'Research Methodologies for Life', 2); -- 4

INSERT INTO StudentSubject (SubId, StudId) VALUES 
(1,1),
(2,2),
(3,3),
(4,4),
(1,5),
(2,6),
(3,7),
(4,8),
(1,2),
(2,3),
(3,4),
(4,5);