﻿using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.ProjectRepo
{
    /// <summary>
    /// Extension of the CRUD Repostory to facilitate any extra, project-specific functionality.
    /// </summary>
    internal interface IProjectRepository : ICrudRepository<Project, int>
    {

    }
}
