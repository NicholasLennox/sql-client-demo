﻿using Microsoft.Data.SqlClient;
using SqlClientDemo.Exceptions;
using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.ProjectRepo
{
    internal class ProjectRepositoryImpl : IProjectRepository
    {
        private readonly string _connectionString;

        public ProjectRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Project obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Project (Title, StudentId) VALUES (@Title, @StudentId)";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Title", obj.Title);
            command.Parameters.AddWithValue("@StudentId", obj.StudentId);
            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "DELETE Project WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

        public List<Project> GetAll()
        {
            List<Project> projects = new List<Project>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Title, StudentId FROM Project";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                projects.Add(new Project(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    ));
            }
            return projects;
        }

        public Project GetById(int id)
        {
            Project project;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Title, StudentId FROM Project WHERE Id = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.NextResult())
            {
                project = new Project(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    );
            }
            else
            {
                throw new ProjectNotFoundException("No project exists with that ID");
            }
            return project;
        }

        public void Update(Project obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Project SET Title = @Title, StudentId = @StudentId WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", obj.Id);
            command.Parameters.AddWithValue("@Title", obj.Title);
            command.Parameters.AddWithValue("@StudentId", obj.StudentId);
            command.ExecuteNonQuery();
        }
    }
}
