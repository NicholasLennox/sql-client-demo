﻿using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.SubjectRepo
{
    internal interface ISubjectRepositoriy : ICrudRepository<Subject, int>
    {
        /// <summary>
        /// Returns a list of subjects a lecturer teaches.
        /// </summary>
        /// <param name="lecturerId"></param>
        /// <returns></returns>
        List<Subject> GetSubjectsForLecturer(int lecturerId);
        /// <summary>
        /// Returns a list of subjects for a student
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        List<Subject> GetSubjectsForStudent(int studentId);
    }
}
