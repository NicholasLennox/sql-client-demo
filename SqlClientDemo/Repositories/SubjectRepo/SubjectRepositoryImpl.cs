﻿using Microsoft.Data.SqlClient;
using SqlClientDemo.Exceptions;
using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.SubjectRepo
{
    internal class SubjectRepositoryImpl : ISubjectRepositoriy
    {
        private readonly string _connectionString;

        public SubjectRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Subject obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Subject (SubCode, SubTitle, LecturerId) VALUES (@SubCode, @SubTitle, @LecturerId)";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@SubCode", obj.SubCode);
            command.Parameters.AddWithValue("@SubTitle", obj.SubTitle);
            command.Parameters.AddWithValue("@LecturerId", obj.LecturerId);
            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "DELETE Subject WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

        public List<Subject> GetAll()
        {
            List<Subject> subjects = new List<Subject>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, SubCode, SubTitle, LecturerId FROM Subject";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                subjects.Add(new Subject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3)
                    ));
            }
            return subjects;
        }

        public Subject GetById(int id)
        {
            Subject subject;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, SubCode, SubTitle, LecturerId FROM Subject WHERE Id = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.NextResult())
            {
                subject = new Subject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3)
                    );
            }
            else
            {
                throw new SubjectNotFoundException("No professor exists with that ID");
            }
            return subject;
        }

        public void Update(Subject obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Subject SET SubCode = @Code, SubTitle = @Title, LecturerId = @Lecturer WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", obj.Id);
            command.Parameters.AddWithValue("@Code", obj.SubCode);
            command.Parameters.AddWithValue("@Title", obj.SubTitle);
            command.Parameters.AddWithValue("@Lecturer", obj.LecturerId);
            command.ExecuteNonQuery();
        }
        public List<Subject> GetSubjectsForLecturer(int lecturerId)
        {
            List<Subject> subjects = new List<Subject>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, SubCode, SubTitle, LecturerId FROM Subject WHERE LecturerId = @LecturerId";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@LecturerId", lecturerId);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                subjects.Add(new Subject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3)
                    ));
            }
            return subjects;
        }

        public List<Subject> GetSubjectsForStudent(int studentId)
        {
            List<Subject> subjects = new List<Subject>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Subject.Id, Subject.SubCode, " +
                "Subject.SubTitle, Subject.LecturerId " +
                "FROM Subject INNER JOIN StudentSubject " +
                "ON Subject.Id = StudentSubject.SubId " +
                "INNER JOIN Student " +
                "ON Student.Id = StudentSubject.StudId " +
                "WHERE Student.Id = @Id;";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", studentId);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                subjects.Add(new Subject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3)
                    ));
            }
            return subjects;
        }
    }
}
