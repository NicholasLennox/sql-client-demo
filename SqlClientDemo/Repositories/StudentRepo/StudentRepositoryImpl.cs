﻿using Microsoft.Data.SqlClient;
using SqlClientDemo.Exceptions;
using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.StudentRepo
{
    internal class StudentRepositoryImpl : IStudentRepository
    {
        private readonly string _connectionString;

        public StudentRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Student obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Student (Name, SupervisorId) VALUES (@Name, @SupervisorId)";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Name", obj.Name);
            command.Parameters.AddWithValue("@SupervisorId", obj.SupervisorId);
            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "DELETE Student WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

        public List<Student> GetAll()
        {
            List<Student> students = new List<Student>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Name, SupervisorId FROM Student";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                students.Add(new Student(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    ));
            }
            return students;
        }

        public Student GetById(int id)
        {
            Student student;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Name, SupervisorId FROM Student WHERE Id = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if (reader.NextResult())
            {
                student = new Student(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetInt32(2)
                    );
            }
            else
            {
                throw new StudentNotFoundException("No student exists with that ID");
            }
            return student;
        }

        public void Update(Student obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Student SET Name = @Name, SupervisorId = @SupervisorId WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", obj.Id);
            command.Parameters.AddWithValue("@Name", obj.Name);
            command.Parameters.AddWithValue("@SupervisorId", obj.SupervisorId);
            command.ExecuteNonQuery();
        }
    }
}
