﻿using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.StudentRepo
{
    /// <summary>
    /// Extension of the CRUD Repostory to facilitate any extra, student-specific functionality.
    /// </summary>
    internal interface IStudentRepository : ICrudRepository<Student, int>
    {
    }
}
