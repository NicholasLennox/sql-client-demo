﻿using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.ProfessorRepo
{
    /// <summary>
    /// Extension of the CRUD Repostory to facilitate any extra, professor-specific functionality.
    /// </summary>
    internal interface IProfessorRepository : ICrudRepository<Professor, int>
    {
    }
}
