﻿using Microsoft.Data.SqlClient;
using Microsoft.Identity.Client;
using SqlClientDemo.Exceptions;
using SqlClientDemo.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Repositories.ProfessorRepo
{
    internal class ProfessorRepositoryImpl : IProfessorRepository
    {
        private readonly string _connectionString;

        public ProfessorRepositoryImpl(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Add(Professor obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "INSERT INTO Professor (Name, Field) VALUES (@Name, @Field)";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Name", obj.Name);
            command.ExecuteNonQuery();
        }

        public void Delete(int id)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "DELETE Professor WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            command.ExecuteNonQuery();
        }

        public List<Professor> GetAll()
        {
            List<Professor> professors = new List<Professor>();
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Name, Field FROM Professor";
            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                professors.Add(new Professor(
                    reader.GetInt32(0), 
                    reader.GetString(1), 
                    reader.GetString(2)
                    ));
            }
            return professors;
        }

        public Professor GetById(int id)
        {
            Professor professor;
            using SqlConnection connection = new SqlConnection(_connectionString);
            connection.Open();
            string sql = "SELECT Id, Name, Field FROM Professor WHERE Id = @Id";
            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@Id", id);
            using SqlDataReader reader = command.ExecuteReader();
            if(reader.NextResult())
            {
                professor = new Professor(
                    reader.GetInt32(0), 
                    reader.GetString(1),
                    reader.GetString(2)
                    );
            } else
            {
                throw new ProfessorNotFoundException("No professor exists with that ID");
            }
            return professor;
        }

        public void Update(Professor obj)
        {
            using SqlConnection connection = new(_connectionString);
            connection.Open();
            string sql = "UPDATE Professor SET Name = @Name, Field = @Field WHERE Id=@Id";
            using SqlCommand command = new(sql, connection);
            command.Parameters.AddWithValue("@Id", obj.Id);
            command.Parameters.AddWithValue("@Name", obj.Name);
            command.Parameters.AddWithValue("@Field", obj.Field);
            command.ExecuteNonQuery();
        }
    }
}
