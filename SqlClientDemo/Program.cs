﻿using Microsoft.Data.SqlClient;
using SqlClientDemo.Repositories.ProfessorRepo;
using SqlClientDemo.Repositories.ProjectRepo;
using SqlClientDemo.Repositories.StudentRepo;
using SqlClientDemo.Repositories.SubjectRepo;
using SqlClientDemo.Util;

namespace SqlClientDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Faking dependency injection
            IProfessorRepository professorRepo = 
                new ProfessorRepositoryImpl(GetConnectionString());
            IStudentRepository studentRepo = 
                new StudentRepositoryImpl(GetConnectionString());
            IProjectRepository projectRepo = 
                new ProjectRepositoryImpl(GetConnectionString());
            ISubjectRepositoriy subjectRepo = 
                new SubjectRepositoryImpl(GetConnectionString());
            DataAccessClient client = 
                new DataAccessClient(
                    professorRepo, 
                    studentRepo, 
                    projectRepo, 
                    subjectRepo);
            //
            client.TestStudentSubjects();
        }
        private static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "ACC-NLENNOX\\SQLEXPRESS";
            builder.InitialCatalog = "PostgradDb";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }
    }
}