﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Models
{
    internal readonly record struct Project(int Id, string Title, int StudentId);
}
