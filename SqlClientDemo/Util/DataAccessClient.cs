﻿using Microsoft.Data.SqlClient;
using SqlClientDemo.Models;
using SqlClientDemo.Repositories.ProfessorRepo;
using SqlClientDemo.Repositories.ProjectRepo;
using SqlClientDemo.Repositories.StudentRepo;
using SqlClientDemo.Repositories.SubjectRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Util
{
    /// <summary>
    /// Client class to do all our data access. Designed to make use of DI.
    /// We fake DI in Program.Main
    /// This class is where you can explore how to use the repositories.
    /// </summary>
    internal class DataAccessClient
    {
        private readonly IProfessorRepository _professorRepo;
        private readonly IStudentRepository _studentRepo;
        private readonly IProjectRepository _projectRepo;
        private readonly ISubjectRepositoriy _subjectRepo;

        public DataAccessClient(IProfessorRepository professorRepo, 
            IStudentRepository studentRepo, 
            IProjectRepository projectRepo, 
            ISubjectRepositoriy subjectRepo)
        {
            _professorRepo = professorRepo;
            _studentRepo = studentRepo;
            _projectRepo = projectRepo;
            _subjectRepo = subjectRepo;
        }

        
        public void DoProfessorDataAccess()
        {
            _professorRepo.GetAll().ForEach(p => Console.WriteLine(p));
            _professorRepo.Add(new Professor(0, "John", "Meth")); // Id is not used for inserts
            _professorRepo.Update(new Professor(1, "Updated", "More Meth"));
            _professorRepo.GetAll().ForEach(p => Console.WriteLine(p));
            _professorRepo.Delete(5);
            _professorRepo.GetAll().ForEach(p => Console.WriteLine(p));
            _professorRepo.GetById(10);
        }

        public void TestStudentSubjects()
        {
            _subjectRepo.GetSubjectsForStudent(2).ForEach(s => Console.WriteLine(s));
        }
    }
}
