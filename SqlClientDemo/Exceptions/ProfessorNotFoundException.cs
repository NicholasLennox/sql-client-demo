﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlClientDemo.Exceptions
{
    internal class ProfessorNotFoundException : Exception
    {
        public ProfessorNotFoundException(string? message) : base(message)
        {
        }
    }
}
